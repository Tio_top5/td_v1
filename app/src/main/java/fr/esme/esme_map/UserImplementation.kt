package fr.esme.esme_map

import fr.esme.esme_map.dao.AppDatabase
import fr.esme.esme_map.interfaces.UserInterface
import fr.esme.esme_map.model.*
import fr.esme.esme_map.MainActivity.MyApplication.Companion.userTemp


class UserImplementation(val user: User, val appDatabase: AppDatabase) : UserInterface {

    override fun getPOIs(): List<POI> {
        return appDatabase.poiDao().getPOIs()
    }

    override fun getRuns(): List<Run> {
        //TODO call network  ===> DB distance ===> DB ====> Code
        return listOf(
            Run(
                user,
                "Footing Paris",
                5,
                listOf(Position(0.0, 0.0)),
                Category("Sport", 12)
            ),
            Run(
                user,
                "Footing Versaille",
                5,
                listOf(Position(0.0, 0.0)),
                Category("Sport", 12)
            )
        )
    }

    override fun getMyPosition(): Position {
        //TODO ask GPS TD
        return Position(2.3929998, 48.8140771)
    }

    override fun getUsers(): List<User> {

        /*
        for (i in userTemp){
            var tmp = userTemp[i].toString()
            var Utmp = User(tmp)
        }
        */


        var riri = User("RIRI")
        var fifi = User("FIFI")
        var loulou = User("LOULOU")
        var popo = User("popo")

        var tmp1 = userTemp[0].toString()
        var usr1 = User(tmp1)
        var tmp2 = userTemp[1].toString()
        var usr2 = User(tmp2)
        var tmp3 = userTemp[2].toString()
        var usr3 = User(tmp3)
        var tmp4 = userTemp[3].toString()
        var usr4 = User(tmp4)

        riri.imageUrl = "https://static.wikia.nocookie.net/haikyuu/images/f/fe/S4.E1.jpg/revision/latest/top-crop/width/300/height/300?cb=20200123205024&path-prefix=fr"
        fifi.imageUrl = "https://www.nautiljon.com/images/perso/00/81/kozume_kenma_10318.jpg"
        popo.imageUrl = "https://static.wikia.nocookie.net/haikyuu/images/4/4a/1.png/revision/latest/smart/width/200/height/200?cb=20150201002127&path-prefix=fr"

        /*popo.imageUrl = "https://www.nautiljon.com/images/perso/00/81/kozume_kenma_10318.jpg"*/

        return listOf(
            riri,
            fifi,
            loulou,
            popo,
            usr1,
            usr2,
            usr3,
            usr4
        )
    }

}


